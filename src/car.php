<?php

class Car
{
    public function __construct()
    {
        session_start();
    }

    public function index()
    {
        if(!isset($_SESSION['cars'])){
            $_SESSION['cars'] =[];
        }

        $cars =$_SESSION['cars'];
        return $cars;
    }

    public function store()
    {
        array_push($_SESSION['cars'], $_POST);
        header('location:index.php');
    }

    public function update()
    {
        $_SESSION['cars'][$_POST['index']] = [
            'id' => $_POST['id'],
            'title' => $_POST['title']
        ];
        header('location:index.php');
    }

    public function delete($id)
    {
        unset($_SESSION['cars'][$id]);
        header('location:index.php');  
    }
}

?>