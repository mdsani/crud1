<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Car</title>
    <!--bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<style>
    .main{
        width:100px; 
        margin: 0 auto; 
        margin-top:30px; 
        margin-bottom:10px;
    }
    table , tr, th, td{
        margin:0 auto; 
        border-collapse: collapse; 
        text-align: center;
        border: 1px solid black;
        padding: 5px;
        margin-top: 10px;
    }
</style>
<body>
<?php

include '../../src/car.php';

$car = new Car;
$cars = $car->index();

?>

    <h2 style="text-align: center; margin-top:50px">Car Model</h2>
    <table class="container">
        <thead>
            <tr>
                <th >SL#</th>
                <th>name</th>
                <th style="width: 100px;"><button style="width: 100%;" type="submit" class="btn btn-primary"><a href="create.php" style="text-decoration: none; color:white;">Add New</a></button></th>
                <th style="width: 100px;"><button style="width: 100%;" type="submit" class="btn btn-primary"><a href="#" style="text-decoration: none; color:white;">Edit</a></button></th>
                <th style="width: 100px;"><button style="width: 100%;" type="submit" class="btn btn-primary"><a href="#" style="text-decoration: none; color:white;">Delete</a></button></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($cars as $key => $car) { ?>

                <tr>
                    <td><?= $car['id'] ?></td>
                    <td><?= $car['title'] ?></td>
                    <td><button style="width: 100%;" type="submit" class="btn btn-primary"><a href="create.php" style="text-decoration: none; color:white;">Add New</a></button></td>
                    <td><button style="width: 100%;" type="submit" class="btn btn-primary"><a href="edit.php?index=<?php echo $key ?>" style="text-decoration: none; color:white;">Edit</a></button></td>
                    <td><button style="width: 100%;" type="submit" class="btn btn-primary"><a href="delete.php?index=<?php echo $key ?>" style="text-decoration: none; color:white;">Delete</a></button></td>
                </tr>

            <?php } ?>
        </tbody>
        
    </table>

    <!--bootstrap js-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>